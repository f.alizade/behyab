/**
 * @format
 */

import {AppRegistry} from 'react-native';
import * as React from 'react';

import App from './src/App';
import Map from './src/Map';
import Redux from './src/Redux';
import Screen1 from './src/Screen1';
import {name as appName} from './app.json';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

function Main() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="app" screenOptions={{headerStyle: {backgroundColor: '#CC0000'},
                    headerTintColor: '#fff',  headerTitleStyle: {fontWeight: 'bold'} }}>
                <Stack.Screen name="app" component={App}  options={{ title: 'first screen' }}
                              initialParams={{ initialnumber: 100 }} />
                <Stack.Screen name="screen1" component={Screen1}  options={{ title: 'Screen1 screen' }}/>
                <Stack.Screen name="map" component={Map}  options={{ title: 'Map screen' }}/>
                <Stack.Screen name="redux" component={Redux}  options={{ title: 'Redux screen' }}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
}

AppRegistry.registerComponent(appName, () => Main);
