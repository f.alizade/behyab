
import React , {useEffect , useState} from 'react';

import {SafeAreaView, StyleSheet, ScrollView, View, Text, StatusBar, TouchableOpacity,} from 'react-native';


function Screen1 ({ navigation }) {

    useEffect(() => {

        const unsubscribe = navigation.addListener('focus', () => {
           console.log('listener is configured')
        });

        return unsubscribe;

    }, [navigation]);

  return (
      <View style={styles.container}>
        <Text>Screen1 Screen</Text>
          <TouchableOpacity style={styles.button1} onPress={()=> navigation.goBack()}>
              <Text>Navigate to Previous screen</Text>
          </TouchableOpacity>
      </View>
  );

}

const styles = StyleSheet.create ({

    container:{ flex: 1, alignItems: 'center', justifyContent: 'space-around' },
    button1:{  alignItems: 'center', justifyContent: 'center' , width:'65%' , height:50 , borderRadius:10 , borderWidth:1 , borderColor:'#CC0000' },

})

export default Screen1;
