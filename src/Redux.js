
import React , {useEffect , useState} from 'react';

import {SafeAreaView, StyleSheet, ScrollView, View, Text, TextInput, TouchableOpacity,} from 'react-native';


function Redux ({ navigation }) {

    useEffect(() => {

        const unsubscribe = navigation.addListener('focus', () => {
           console.log('listener is configured')
        });

        return unsubscribe;

    }, [navigation]);

  return (
      <View style={styles.container}>

        <Text>Counter: 0</Text>
         <View style = {styles.floatingView}>
           <TouchableOpacity style ={styles.floatingButton}>
             <Text>+</Text>
           </TouchableOpacity>
           <TouchableOpacity style ={styles.floatingButton}>
             <Text>-</Text>
           </TouchableOpacity>
         </View>
         <TextInput keyboardType = 'numeric' placeholder= "change                    amount"/> 
     
      </View>
  );

}

const styles = StyleSheet.create ({

    container:{ flex: 1, alignItems: 'center', justifyContent: 'space-around' },
    button1:{  alignItems: 'center', justifyContent: 'center' , width:'65%' , height:50 , borderRadius:10 , borderWidth:1 , borderColor:'#CC0000' },

})

export default Redux;
