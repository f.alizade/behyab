
import React , {useEffect , useState} from 'react';

import {SafeAreaView, StyleSheet, ScrollView, View, Text, StatusBar, TouchableOpacity,} from 'react-native';

import MapView from 'react-native-maps';


function Map ({ navigation }) {

    useEffect(() => {

        const unsubscribe = navigation.addListener('focus', () => {
        //    console.log('listener is configured')
        });

        return unsubscribe;

    }, [navigation]);
 
  return (
      <View style={styles.container}>
        <MapView
            region={{
            latitude: 35.68825,
            longitude: 51.4884,
            latitudeDelta: 0.03,
            longitudeDelta: 0.03,
            }}
            style={{height:'100%' , width:'100%'}}

            mapType = 'standard'

        />
      </View>
  );

}

const styles = StyleSheet.create ({

    container:{ flex: 1, alignItems: 'center', justifyContent: 'space-around' },
    button1:{  alignItems: 'center', justifyContent: 'center' , width:'65%' , height:50 , borderRadius:10 , borderWidth:1 , borderColor:'#CC0000' },

})

export default Map;
