import React, { useState, useEffect } from "react";

import { TouchableOpacity, StyleSheet, View, Text, } from 'react-native';

import socketIOClient from "socket.io-client";

const ENDPOINT = "http://192.168.20.118:3051?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OTEsIkpUSSI6InhEQU1hIiwidHlwZSI6InVzZXIifQ.rLEg5uFuVzCI1zbND9ZObBLMiA0qTFPbu010beWHOKU";

var socket = socketIOClient(ENDPOINT);

function App ({ navigation, route }) {
  
  useEffect(() => {


    socket.on('auth', result => {
      console.log(result);
    });

    // console.log(socket)

  }, []);

  return (
      <View style={styles.container}>
          <Text>First Screen</Text>
          <Text> initial number is: {route.params?.initialnumber}</Text>

          <TouchableOpacity style={styles.button1} onPress={()=> navigation.navigate('redux')}>
              <Text>Navigate to Next screen</Text>
          </TouchableOpacity>
      </View>
  );

}

const styles = StyleSheet.create ({

  container:{ flex: 1, alignItems: 'center', justifyContent: 'space-around' },
    button1:{  alignItems: 'center', justifyContent: 'center' , width:'65%' , height:50 , borderRadius:10 , borderWidth:1 , borderColor:'#CC0000' },

});

export default App;
